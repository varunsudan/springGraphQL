# SpringGraphQL

Sample GraphQL application built using spring boot graphql starter.

## Download

```bash
git clone git@gitlab.com:varunsudan/springGraphQL.git
```

## Run

```
$ cd springGraphQL
$ ./gradlew bootRun
```

## Verify
1> go to http://localhost:8080/graphiql
2> run the query as shown in the image below :
![](graphql_query_image.png)

## Ref
https://www.pluralsight.com/guides/building-a-graphql-server-with-spring-boot