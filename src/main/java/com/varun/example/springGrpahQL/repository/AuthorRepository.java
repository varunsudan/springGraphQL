package com.varun.example.springGrpahQL.repository;

import com.varun.example.springGrpahQL.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Integer> { }
