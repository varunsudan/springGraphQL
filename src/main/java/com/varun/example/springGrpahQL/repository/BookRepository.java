package com.varun.example.springGrpahQL.repository;

import com.varun.example.springGrpahQL.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Integer> { }
