package com.varun.example.springGrpahQL.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.varun.example.springGrpahQL.model.Author;
import com.varun.example.springGrpahQL.model.Book;
import com.varun.example.springGrpahQL.repository.AuthorRepository;

import java.util.Optional;

public class BookResolver implements GraphQLResolver<Book> {
    private AuthorRepository authorRepository;

    public BookResolver(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public Optional<Author> getAuthor(Book book) {
        return authorRepository.findById(book.getAuthor().getId());
    }
}
