package com.varun.example.springGrpahQL.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.varun.example.springGrpahQL.error.BookNotFoundException;
import com.varun.example.springGrpahQL.model.Author;
import com.varun.example.springGrpahQL.model.Book;
import com.varun.example.springGrpahQL.repository.AuthorRepository;
import com.varun.example.springGrpahQL.repository.BookRepository;

import java.util.Optional;

public class Mutation implements GraphQLMutationResolver {
    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    public Mutation(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    public Author newAuthor(String firstName, String lastName) {
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);

        authorRepository.save(author);

        return author;
    }

    public Book newBook(String title, String isbn, Integer pageCount, Integer authorId) {
        Book book = new Book();
        book.setAuthor(new Author(authorId));
        book.setTitle(title);
        book.setIsbn(isbn);
        book.setPageCount(pageCount != null ? pageCount : 0);

        bookRepository.save(book);

        return book;
    }

    public boolean deleteBook(Integer id) {
        bookRepository.deleteById(id);
        return true;
    }

    public Book updateBookPageCount(Integer pageCount, Integer id) {
        Optional<Book> book = bookRepository.findById(id);
        if(!book.isPresent()) {
            throw new BookNotFoundException("The book to be updated was not found", id);
        }
        book.get().setPageCount(pageCount);

        bookRepository.save(book.get());

        return book.get();
    }
}
