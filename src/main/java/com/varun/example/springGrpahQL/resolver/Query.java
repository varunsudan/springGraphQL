package com.varun.example.springGrpahQL.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.varun.example.springGrpahQL.model.Author;
import com.varun.example.springGrpahQL.model.Book;
import com.varun.example.springGrpahQL.repository.AuthorRepository;
import com.varun.example.springGrpahQL.repository.BookRepository;

public class Query implements GraphQLQueryResolver {

    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    public Query(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    public Iterable<Book> findAllBooks() {
        return bookRepository.findAll();
    }

    public Iterable<Author> findAllAuthors() {
        return authorRepository.findAll();
    }

    public long countBooks() {
        return bookRepository.count();
    }
    public long countAuthors() {
        return authorRepository.count();
    }
}
